# Project 1 starter program for calculating pi
import numpy

#This program calculates pi using the Monte Carlo Method
#By throwing darts at a square dartboard and counting
#The darts that go in the circle vs. the total number of darts =pi/4

#How to throw darts at a square:
def throw_dart():
        x = numpy.random.random() #each of the values generated should be between 0 and 1
        y = numpy.random.random()
        return x,y

#To determine if the dart is in the circle or square:

n = 0
measurement = []
def in_circle(point):
       	#r=sqrt(x^2 + y^2)
	r = (point[0]**2 + point[1]**2)**(0.5)
	if r <= 1:
        	return 1
	else:
		return 0

num_runs = input("We will use a function to determine PI based on the \nfraction of darts landing inside a circle vs how many land outside.\nHow many darts do you want to throw? ")

# This function asks the user how many darts they would like to throw at the board. The more darts thrown the more accurate the calculation will be.

total_runs = int(num_runs)

# This loop iterates over the total number of runs.
for i in range(total_runs):
#If r<1, the point lies within the circle.
#If r>1, the point lies outside of the circle.
	if (in_circle(throw_dart())) == 1:
		n += 1
	measurement.append(n)
		
pi = 4*numpy.array(measurement)/total_runs

print("The calculated value of pi is " , pi)
#Call my functions down here

#The function below calculates our % error for our pi calculation

pi_true = 3.1415926535897
error = abs(((pi - pi_true)/(pi_true))*100)
print("The % error is " , error)
	 

# lineplot.py 
import numpy  
import matplotlib.pyplot as plt  

# Make an array of x values 
x= range(total_runs)
 
# Make an array of y values for each x value 
y= abs(((pi-pi_true)/(pi_true)*100))

#use pylab to plot x and y 
plt.plot(x,y,'r+')
plt.xlabel('Number of darts thrown')

# show the plot on the screen 
plt.show() 
print("DONE")   
