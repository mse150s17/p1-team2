# README #

ETHERPAD: https://etherpad.net/p/Project_1_Team_2

This README will be for Team 2's Project 1 repository for calculating pi.

### What is this repository for? ###

   This repository is to help explain how to use the program for calculating pi. It contains step-by-step instructions on how to use the program. The program works by generating points within a square. In that square is a circle with the same diameter of the square.  Some points fall within the square and some the circle. The program calculates the ratio of the darts within the circle to the ones outside the circle. 

This is Version 1.0 
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get started? ###

* Summary of set up:
   We are trying to calculate pi using the Monte Carlo method

* Configuration:
   The test consists of generating random points within a square. Inside that square is a circle with a diameter equal to the size of the square.
  
* Dependencies:
   Test is dependent on the number of points generated. The more points generated the more accurate the test.
	
* Database configuration
   If a point is less than 1 it is inside the circle, more than 1 outside the circle.
   
* How to run tests:
    To start running tests you first need to launch the program. To do this you need to access the program through python.
	Type: python t2mc.py 
This will run the program.  A prompt should appear asking "how many darts do you want to throw"
	Enter the number of darts you wish.
    The program will then throw that number of darts and calculate pi.  The calculation will appear along with the % error for that calculation.

* Deployment instructions:
   See how to run tests for deployment instructions

### Contribution guidelines ###

* Writing tests
   To write tests....   
   	
* Code review
   Information on the code used to calculate pi can be found online via a Google search for
   "python calculating pi Monte Carlo method"

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
  Questions should be directed to Team 2.  Contact information can be found under the Emails.txt file. 

* Other community or team contact